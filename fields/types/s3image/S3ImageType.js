var _ = require('lodash');
var FieldType = require('../Type');
var util = require('util');
var utils = require('keystone-utils');
var assign = require('object-assign');
var ensureCallback = require('keystone-storage-namefunctions/ensureCallback');
var nameFunctions = require('keystone-storage-namefunctions');


var debug = require('debug')('keystone:fields:s3image');

/**
 * File FieldType Constructor
 */
 var DEFAULT_OPTIONS = {
	// This makes Cloudinary assign a unique public_id and is the same as
	//   the legacy implementation
	generateFilename: () => undefined,
	whenExists: 'overwrite',
	retryAttempts: 3, // For whenExists: 'retry'.
};

function getEmptyValue () {
	return {
		public_id: '',
		version: 0,
		signature: '',
		format: '',
		resource_type: '',
		url: '',
		width: 0,
		height: 0,
		secure_url: '',
	};
}

function s3image (list, path, options) {
	this._underscoreMethods = ['format'];
	this._fixedSize = 'full';
	this._properties = ['select', 'selectPrefix', 'autoCleanup'];

	if (options.filenameAsPublicID) {
		// Produces the same result as the legacy filenameAsPublicID option
		options.generateFilename = nameFunctions.originalFilename;
		options.whenExists = 'overwrite';
	}
	options = assign({}, DEFAULT_OPTIONS, options);
	options.generateFilename = ensureCallback(options.generateFilename);

	s3image.super_.call(this, list, path, options);
}

s3image.properName = 'S3Image';
util.inherits(s3image, FieldType);

/**
 * Registers the field on the List's Mongoose Schema.
 */
s3image.prototype.addToSchema = function (schema) {

	var knox = require('knox-s3');
	var field = this;

	var paths = this.paths = {
		// fields
		filename: this.path + '.filename',
		originalname: this.path + '.originalname',
		path: this.path + '.path',
		size: this.path + '.size',
		filetype: this.path + '.filetype',
		url: this.path + '.url',
		// virtuals
		exists: this.path + '.exists',
		upload: this.path + '_upload',
		action: this.path + '_action',
	};

	var schemaPaths = this._path.addTo({}, {
		filename: String,
		originalname: String,
		path: String,
		size: Number,
		filetype: String,
		url: String,
	});

	schema.add(schemaPaths);

	var exists = function (item) {
		return (item.get(paths.url) ? true : false);
	};

	// The .exists virtual indicates whether a file is stored
	schema.virtual(paths.exists).get(function () {
		return schemaMethods.exists.apply(this);
	});

	var reset = function (item) {
		item.set(field.path, {
			filename: '',
			originalname: '',
			path: '',
			size: 0,
			filetype: '',
			url: '',
		});
	};

	var schemaMethods = {
		exists: function () {
			return exists(this);
		},
		/**
		 * Resets the value of the field
		 *
		 * @api public
		 */
		reset: function () {
			reset(this);
		},
		/**
		 * Deletes the file from S3File and resets the field
		 *
		 * @api public
		 */
		delete: function () {
			try {
				var client = knox.createClient(field.s3config);
				client.deleteFile(this.get(paths.path) + this.get(paths.filename), function (err, res) { return res ? res.resume() : false; }); // eslint-disable-line handle-callback-err
			} catch (e) {} // eslint-disable-line no-empty
			reset(this);
		},
	};

	_.forEach(schemaMethods, function (fn, key) {
		field.underscoreMethod(key, fn);
	});

	// expose a method on the field to call schema methods
	this.apply = function (item, method) {
		return schemaMethods[method].apply(item, Array.prototype.slice.call(arguments, 2));
	};

	this.bindUnderscoreMethods();
};

/**
 * Uploads a new file
 */
s3image.prototype.upload = function (item, file, callback) {
	var field = this;
	// TODO; Validate there is actuall a file to upload
	debug('[%s.%s] Uploading file for item %s:', this.list.key, this.path, item.id, file);
	this.storage.uploadFile(file, function (err, result) {
		if (err) return callback(err);
		debug('[%s.%s] Uploaded file for item %s with result:', field.list.key, field.path, item.id, result);
		item.set(field.path, result);
		callback(null, result);
	});
};

/**
 * Resets the field value
 */
s3image.prototype.reset = function (item) {
	var value = {};
	Object.keys(this.storage.schema).forEach(function (path) {
		value[path] = null;
	});
	item.set(this.path, value);
};

/**
 * Deletes the stored file and resets the field value
 */
// TODO: Should we accept a callback here? Seems like a good idea.
s3image.prototype.remove = function (item) {
	this.storage.removeFile(item.get(this.path));
	this.reset();
};

/**
 * Formats the field value
 */
s3image.prototype.format = function (item) {
	var value = item.get(this.path);
	if (value) return value.filename || '';
	return '';
};

/**
 * Detects whether the field has been modified
 */
s3image.prototype.isModified = function (item) {
	var modified = false;
	var paths = this.paths;
	Object.keys(this.storageSchema).forEach(function (path) {
		if (item.isModified(paths[path])) modified = true;
	});
	return modified;
};


function validateInput (value) {
	// undefined, null and empty values are always valid
	if (value === undefined || value === null || value === '') return true;
	// If a string is provided, check it is an upload or delete instruction
	if (typeof value === 'string' && /^(upload\:)|(delete$)/.test(value)) return true;
	// If the value is an object with a filename property, it is a stored value
	// TODO: Need to actually check a dynamic path based on the adapter
	if (typeof value === 'object' && value.filename) return true;
	return false;
}

/**
 * Validates that a value for this field has been provided in a data object
 */
s3image.prototype.validateInput = function (data, callback) {
	var value = this.getValueFromData(data);
	debug('[%s.%s] Validating input: ', this.list.key, this.path, value);
	var result = validateInput(value);
	debug('[%s.%s] Validation result: ', this.list.key, this.path, result);
	utils.defer(callback, result);
};

/**
 * Validates that input has been provided
 */
s3image.prototype.validateRequiredInput = function (item, data, callback) {
	// TODO: We need to also get the `files` argument, so we can check for
	// uploaded files. without it, this will return false negatives so we
	// can't actually validate required input at the moment.
	var result = true;
	// var value = this.getValueFromData(data);
	// debug('[%s.%s] Validating required input: ', this.list.key, this.path, value);
	// TODO: Need to actually check a dynamic path based on the adapter
	// TODO: This incorrectly allows empty values in the object to pass validation
	// var result = (value || item.get(this.paths.filename)) ? true : false;
	// debug('[%s.%s] Validation result: ', this.list.key, this.path, result);
	utils.defer(callback, result);
};

/**
 * Updates the value for this field in the item from a data object
 * TODO: It is not possible to remove an existing value and upload a new fiel
 * in the same action, this should be supported
 */
s3image.prototype.updateItem = function (item, data, files, callback) {
	// Process arguments
	if (typeof files === 'function') {
		callback = files;
		files = {};
	}
	if (!files) {
		files = {};
	}

	// Prepare values
	var value = this.getValueFromData(data);
	var uploadedFile;

	// Providing the string "remove" removes the file and resets the field
	if (value === 'remove') {
		this.remove(item);
		utils.defer(callback);
	}

	// Find an uploaded file in the files argument, either referenced in the
	// data argument or named with the field path / field_upload path + suffix
	if (typeof value === 'string' && value.substr(0, 7) === 'upload:') {
		uploadedFile = files[value.substr(7)];
	} else {
		uploadedFile = this.getValueFromData(files) || this.getValueFromData(files, '_upload');
	}

	// Ensure a valid file was uploaded, else null out the value
	if (uploadedFile && !uploadedFile.path) {
		uploadedFile = undefined;
	}

	// If we have a file to upload, we do that and stop here
	if (uploadedFile) {
		return this.upload(item, uploadedFile, callback);
	}

	// Empty / null values reset the field
	if (value === null || value === '' || (typeof value === 'object' && !Object.keys(value).length)) {
		this.reset(item);
		value = undefined;
	}

	// If there is a valid value at this point, set it on the field
	if (typeof value === 'object') {
		item.set(this.path, value);
	}
	utils.defer(callback);
};

/* Export Field Type */
module.exports = s3image;
